<?php

namespace Monzatan\Acommerce\Block;

use Monzatan\Acommerce\Helper\Data as HelperData;
use Psr\Log\LoggerInterface;

class Test extends \Magento\Framework\View\Element\Template
{
    /**
     * @var HelperData
     */
    private $helperData;

    /**
     * @var Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * Test constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     * @param HelperData $helperData
     * @param Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = [],
        HelperData $helperData,
        LoggerInterface $logger
    )
    {
        parent::__construct($context);
        $this->helperData = $helperData;
        $this->logger = $logger;
    }

    /**
     * @return mixed
     */
    public function getListJson()
    {
        return json_decode($this->helperData->getFileContents(), true);
    }

    /**
     * @param $datetime
     * @return string
     * @throws \Exception
     */
    public function calculateDateDiffWithCurrentDate($datetime)
    {
        try {
            $currentDate = new \DateTime();
            $datetimeObj = new \DateTime($datetime);
            $interval = $currentDate->diff($datetimeObj);

            if ($interval->y > 0) {
                return $interval->y . ' year' . $this->isMoreThanOne($interval->y) . ' ago';
            }

            if ($interval->m > 0) {
                return $interval->m . ' month' . $this->isMoreThanOne($interval->m) . ' ago';
            }

            if ($interval->d > 0) {
                return $interval->d . ' day' . $this->isMoreThanOne($interval->d) . ' ago';
            }

            if ($interval->h > 0) {
                return $interval->h . ' hour' . $this->isMoreThanOne($interval->h) . ' ago';
            }

            if ($interval->i > 0) {
                return $interval->i . ' minute' . $this->isMoreThanOne($interval->i) . ' ago';
            }

            if ($interval->s > 0) {
                return $interval->s . ' second' . $this->isMoreThanOne($interval->s) . ' ago';
            }

            return '-';
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return '-';
        }
    }

    public function isMoreThanOne($value)
    {
        return $value > 1 ? 's' : '';
    }

    public function getStarRating($stars, $max = 5)
    {
        $html = '';

        try {
            $i = 1;
            while ($i <= $max) {
                if ($i <= $stars) {
                    $html .= '<span class="fa fa-star checked"></span>';
                } else {
                    $html .= '<span class="fa fa-star-o"></span>';
                }

                $i++;
            }

            return $html;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            return $html;
        }
    }
}
