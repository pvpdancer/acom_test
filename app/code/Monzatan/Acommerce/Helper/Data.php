<?php
namespace Monzatan\Acommerce\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;

class Data
{
    private $directoryList;
    private $driverFile;
    private $logger;

    public  function __construct(
        \Magento\Framework\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Filesystem\Driver\File $driverFile,
        \Psr\Log\LoggerInterface $logger)
    {
        $this->directoryList = $directoryList;
        $this->driverFile = $driverFile;
        $this->logger = $logger;
    }

    public function getFileContents() {
        $contents = null;
        try {
            $path = $this->directoryList->getPath(DirectoryList::MEDIA).'/acommerce/list.json';

            $contents = $this->driverFile->fileGetContents($path);
        } catch (FileSystemException $e) {
            $this->logger->error($e->getMessage());
        }

        return $contents;
    }
}
